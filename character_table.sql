USE [RPG-generator]
GO

/****** Object:  Table [dbo].[Character]    Script Date: 24/08/2020 13:25:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Character](
	[character_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[class_id] [int] NULL,
	[hp_max] [int] NULL,
	[hp] [int] NULL,
	[stam_max] [int] NULL,
	[stam] [int] NULL,
	[mana_max] [int] NULL,
	[mana] [int] NULL,
	[vitality] [int] NULL,
	[endurance] [int] NULL,
	[intellect] [int] NULL,
	[strength] [int] NULL,
	[agility] [int] NULL,
 CONSTRAINT [PK_Character] PRIMARY KEY CLUSTERED 
(
	[character_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

