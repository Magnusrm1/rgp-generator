﻿using RPGClassLib;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_generator
{
    public class DBHelper
    {
        public Dictionary<int, string> classes = new Dictionary<int, string>()
            {
                { 0, "Berserker" },
                { 1, "Conjurer" },
                { 2, "Face stabber" },
                { 3, "Fire mage" },
                { 4, "Knight" },
                { 5, "Ranger" }
            };
        private SqlConnection Connect()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7363\\SQLEXPRESS";
            builder.InitialCatalog = "RPG-generator";
            builder.IntegratedSecurity = true;

            return new SqlConnection(builder.ConnectionString);
        }

        public void AddCharacter(Character character)
        {
            string sql = "INSERT INTO Character (name, class_id, hp_max, hp, stam_max, stam, " +
                                                 "mana_max, mana, vitality, endurance, intellect, " +
                                                 "strength, agility)" +
                         "VALUES (@Name, @ClassID, @HpMax, @Hp, @StamMax, @Stam, " +
                                 "@ManaMax, @Mana, @Vitality, @Endurance, @Intellect, " +
                                 "@Strength, @Agility);";
            try
            {
                using (SqlConnection connection = Connect())
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Name", character.Name);
                        command.Parameters.AddWithValue("@ClassID", character.ClassID);
                        command.Parameters.AddWithValue("@HpMax", character.HpMax);
                        command.Parameters.AddWithValue("@Hp", character.Hp);
                        command.Parameters.AddWithValue("@StamMax", character.StaminaMax);
                        command.Parameters.AddWithValue("@Stam", character.Stamina);
                        command.Parameters.AddWithValue("@ManaMax", character.ManaMax);
                        command.Parameters.AddWithValue("@Mana", character.Mana);
                        command.Parameters.AddWithValue("@Vitality", character.Vitality);
                        command.Parameters.AddWithValue("@Endurance", character.Endurance);
                        command.Parameters.AddWithValue("@Intellect", character.Intellect);
                        command.Parameters.AddWithValue("@Strength", character.Strength);
                        command.Parameters.AddWithValue("@Agility", character.Agility);

                        command.ExecuteNonQuery();
                    }
                }
            } catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public Character FetchCharacter(int characterID)
        {
            string sql = "SELECT * " +
                         "FROM Character " +
                         "WHERE character_id = @CharacterID";
            try
            {
                using (SqlConnection connection = Connect())
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CharacterID", characterID);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return CreateCharacter(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(9),
                                   reader.GetInt32(10), reader.GetInt32(11), reader.GetInt32(12),
                                   reader.GetInt32(13), reader.GetInt32(2));
                        }
                    }
                }
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
            
        }

        public List<Character> FetchAllCharacters()
        {
            List<Character> result = new List<Character>();
            string sql = "SELECT * " +
                         "FROM Character";
            try
            {
                using (SqlConnection connection = Connect())
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            Character temp;
                            while (reader.Read())
                            {
                                temp = CreateCharacter(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(9),
                                   reader.GetInt32(10), reader.GetInt32(11), reader.GetInt32(12),
                                   reader.GetInt32(13), reader.GetInt32(2));
                                result.Add(temp);
                            }
                            return result;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public void UpdateCharacter(Character character)
        {
            string sql = "UPDATE Character " +
                         "SET name = @Name, class_id = @ClassID, hp_max = @HpMax, hp = @Hp, " +
                         "stam_max = @StamMax, stam = @Stam, mana_max = @ManaMax, mana = @Mana, " +
                         "vitality = @Vitality, endurance = @Endurance, intellect = @Intellect, " +
                         "strength = @Strength, agility = @Agility) " +
                         "WHERE character_id = @CharacterID;";
            try
            {
                using (SqlConnection connection = Connect())
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Name", character.Name);
                        command.Parameters.AddWithValue("@ClassID", character.ClassID);
                        command.Parameters.AddWithValue("@HpMax", character.HpMax);
                        command.Parameters.AddWithValue("@Hp", character.Hp);
                        command.Parameters.AddWithValue("@StamMax", character.StaminaMax);
                        command.Parameters.AddWithValue("@Stam", character.Stamina);
                        command.Parameters.AddWithValue("@ManaMax", character.ManaMax);
                        command.Parameters.AddWithValue("@Mana", character.Mana);
                        command.Parameters.AddWithValue("@Vitality", character.Vitality);
                        command.Parameters.AddWithValue("@Endurance", character.Endurance);
                        command.Parameters.AddWithValue("@Intellect", character.Intellect);
                        command.Parameters.AddWithValue("@Strength", character.Strength);
                        command.Parameters.AddWithValue("@Agility", character.Agility);
                        command.Parameters.AddWithValue("@CharacterID", character.CharacterID);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void DeleteCharacter(int characterID)
        {
            string sql = "DELETE " +
                         "FROM Character " +
                         "WHERE character_id = @CharacterID";
            try
            {
                using (SqlConnection connection = Connect())
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CharacterID", characterID);

                        command.ExecuteReader();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public Character CreateCharacter(string name, int vit, int end, int intellect, int str, int agi, int classID)
        {
            switch (classID)
            {
                case 0:
                    return new Berserker(name, vit, end, intellect, str, agi);
                case 1:
                    return new Conjurer(name, vit, end, intellect, str, agi);
                case 2:
                    return new FaceStabber(name, vit, end, intellect, str, agi);
                case 3:
                    return new FireMage(name, vit, end, intellect, str, agi);
                case 4:
                    return new Knight(name, vit, end, intellect, str, agi);
                case 5:
                    return new Ranger(name, vit, end, intellect, str, agi);
                default:
                    Debug.WriteLine("No such Class ID");
                    return null;
            }
        }

        public Character CreateCharacter(int characterID, string name, int vit, int end, int intellect, int str, int agi, int classID)
        {
            switch (classID)
            {
                case 0:
                    return new Berserker(characterID, name, vit, end, intellect, str, agi);
                case 1:
                    return new Conjurer(characterID, name, vit, end, intellect, str, agi);
                case 2:
                    return new FaceStabber(characterID, name, vit, end, intellect, str, agi);
                case 3:
                    return new FireMage(characterID, name, vit, end, intellect, str, agi);
                case 4:
                    return new Knight(characterID, name, vit, end, intellect, str, agi);
                case 5:
                    return new Ranger(characterID, name, vit, end, intellect, str, agi);
                default:
                    Debug.WriteLine("No such Class ID");
                    return null;
            }
        }
    }
}
