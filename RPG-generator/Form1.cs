﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPGClassLib;
using CsvHelper;
using System.Diagnostics;

namespace RPG_generator
{
    public partial class Form1 : Form
    {
        public List<Character> characters;

        public DBHelper dbHelper = new DBHelper();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (tbName.Text != "" && tbName.Text != null)
            {
                // Create Character object of correct class type using form values.
                Character newCha = dbHelper.CreateCharacter(tbName.Text, Convert.ToInt32(nudVit.Value), Convert.ToInt32(nudEnd.Value), Convert.ToInt32(nudInt.Value), Convert.ToInt32(nudStr.Value), Convert.ToInt32(nudAgi.Value), cbmClassCreate.SelectedIndex);

                // Save in database.
                dbHelper.AddCharacter(newCha);
                // Update display.
                this.Form1_Load(this, e);
            }

            else
            {

            }
            

        }

        private void lbCharacters_SelectedIndexChanged(object sender, EventArgs e)
        {
            // get character object from the selected list item
            if (lbCharacters.SelectedItem != null && lbCharacters.SelectedItem is Character) {
                Character character = (Character)lbCharacters.SelectedItem;

                // display stats of the Character
                DisplayCharacter(character);
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cbmClassCreate.ValueMember = "Key";
            cbmClassCreate.DisplayMember = "Value";

            // Fill combobox with classes defined in dbHelper.
            cbmClassCreate.DataSource = new BindingSource(dbHelper.classes, null);

            // Get all stored characters to load characters into character list.
            characters = dbHelper.FetchAllCharacters();

            lbCharacters.DataSource = characters;
            lbCharacters.DisplayMember = "Name";

        }

        
        // Method to save a charater in csv file.
        private void saveCharacterLocally(Character character)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter("Characters.csv", false, Encoding.UTF8))
                {
                    using (CsvWriter csv = new CsvWriter(writer, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        // Write character to csv.
                        csv.WriteHeader<Character>();
                        csv.NextRecord();
                        csv.WriteRecord<Character>(character);
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        // Method to get a list of all characters saved in the local csv file.
        private List<Character> savedCharacters()
        {
            List<Character> result = new List<Character>();
            
            try
            {
                using (StreamReader reader = new StreamReader("Characters.csv"))
                {
                    using (CsvReader csv = new CsvReader(reader, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        IEnumerable<Character> characters = csv.GetRecords<Character>();

                        // Add each character in records to result.
                        foreach (Character character in characters)
                        {
                            result.Add(character);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return result;
        }

        // Method to display a characters stats 
        public void DisplayCharacter(Character character)
        {

            lblName.Text = character.Name;
            lblClass.Text = dbHelper.classes[character.ClassID];
            lblHPShow.Text = character.Hp.ToString() + "/" + character.HpMax.ToString();
            lblStaminaShow.Text = character.Stamina.ToString() + "/" + character.StaminaMax.ToString();
            lblManaShow.Text = character.Mana.ToString() + "/" + character.ManaMax.ToString();
            lblVitShow.Text = character.Vitality.ToString();
            lblEndShow.Text = character.Endurance.ToString();
            lblIntShow.Text = character.Intellect.ToString();
            lblStrShow.Text = character.Strength.ToString();
            lblAgiShow.Text = character.Agility.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lbCharacters.SelectedItem != null && lbCharacters.SelectedItem is Character)
            {
                // Retrieve the character to be deleted.
                Character character = (Character)lbCharacters.SelectedItem;
                // Delete the character from database.
                dbHelper.DeleteCharacter(character.CharacterID);
                // Update display.
                this.Form1_Load(this, e);
            }

        }
    }
}
