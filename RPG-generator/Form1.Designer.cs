﻿namespace RPG_generator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeading = new System.Windows.Forms.Label();
            this.lblAddName = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.lbCharacters = new System.Windows.Forms.ListBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.cbmClassCreate = new System.Windows.Forms.ComboBox();
            this.lblVitCreate = new System.Windows.Forms.Label();
            this.lblAgiCreate = new System.Windows.Forms.Label();
            this.lblIntCreate = new System.Windows.Forms.Label();
            this.lblEndCreate = new System.Windows.Forms.Label();
            this.lblStrCreate = new System.Windows.Forms.Label();
            this.nudVit = new System.Windows.Forms.NumericUpDown();
            this.nudEnd = new System.Windows.Forms.NumericUpDown();
            this.nudInt = new System.Windows.Forms.NumericUpDown();
            this.nudStr = new System.Windows.Forms.NumericUpDown();
            this.nudAgi = new System.Windows.Forms.NumericUpDown();
            this.lblName = new System.Windows.Forms.Label();
            this.lblVit = new System.Windows.Forms.Label();
            this.lblAgi = new System.Windows.Forms.Label();
            this.lblInt = new System.Windows.Forms.Label();
            this.lblEnd = new System.Windows.Forms.Label();
            this.lblClass = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblStr = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblVitShow = new System.Windows.Forms.Label();
            this.lblIntShow = new System.Windows.Forms.Label();
            this.lblEndShow = new System.Windows.Forms.Label();
            this.lblStrShow = new System.Windows.Forms.Label();
            this.lblAgiShow = new System.Windows.Forms.Label();
            this.lblHPShow = new System.Windows.Forms.Label();
            this.lblStaminaShow = new System.Windows.Forms.Label();
            this.lblManaShow = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudVit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAgi)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.lblHeading.Location = new System.Drawing.Point(190, 12);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(326, 37);
            this.lblHeading.TabIndex = 0;
            this.lblHeading.Text = "Create your character";
            // 
            // lblAddName
            // 
            this.lblAddName.AutoSize = true;
            this.lblAddName.Location = new System.Drawing.Point(193, 69);
            this.lblAddName.Name = "lblAddName";
            this.lblAddName.Size = new System.Drawing.Size(55, 20);
            this.lblAddName.TabIndex = 1;
            this.lblAddName.Text = "Name:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(268, 66);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(168, 26);
            this.tbName.TabIndex = 2;
            // 
            // lbCharacters
            // 
            this.lbCharacters.FormattingEnabled = true;
            this.lbCharacters.ItemHeight = 20;
            this.lbCharacters.Location = new System.Drawing.Point(12, 12);
            this.lbCharacters.Name = "lbCharacters";
            this.lbCharacters.Size = new System.Drawing.Size(154, 424);
            this.lbCharacters.TabIndex = 3;
            this.lbCharacters.SelectedIndexChanged += new System.EventHandler(this.lbCharacters_SelectedIndexChanged);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(314, 395);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 37);
            this.btnCreate.TabIndex = 4;
            this.btnCreate.Text = "Create!";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // cbmClassCreate
            // 
            this.cbmClassCreate.Location = new System.Drawing.Point(268, 110);
            this.cbmClassCreate.Name = "cbmClassCreate";
            this.cbmClassCreate.Size = new System.Drawing.Size(168, 28);
            this.cbmClassCreate.TabIndex = 8;
            // 
            // lblVitCreate
            // 
            this.lblVitCreate.AutoSize = true;
            this.lblVitCreate.Location = new System.Drawing.Point(288, 190);
            this.lblVitCreate.Name = "lblVitCreate";
            this.lblVitCreate.Size = new System.Drawing.Size(59, 20);
            this.lblVitCreate.TabIndex = 9;
            this.lblVitCreate.Text = "Vitality:";
            // 
            // lblAgiCreate
            // 
            this.lblAgiCreate.AutoSize = true;
            this.lblAgiCreate.Location = new System.Drawing.Point(293, 317);
            this.lblAgiCreate.Name = "lblAgiCreate";
            this.lblAgiCreate.Size = new System.Drawing.Size(54, 20);
            this.lblAgiCreate.TabIndex = 10;
            this.lblAgiCreate.Text = "Agility:";
            // 
            // lblIntCreate
            // 
            this.lblIntCreate.AutoSize = true;
            this.lblIntCreate.Location = new System.Drawing.Point(278, 252);
            this.lblIntCreate.Name = "lblIntCreate";
            this.lblIntCreate.Size = new System.Drawing.Size(69, 20);
            this.lblIntCreate.TabIndex = 11;
            this.lblIntCreate.Text = "Intellect:";
            // 
            // lblEndCreate
            // 
            this.lblEndCreate.AutoSize = true;
            this.lblEndCreate.Location = new System.Drawing.Point(256, 221);
            this.lblEndCreate.Name = "lblEndCreate";
            this.lblEndCreate.Size = new System.Drawing.Size(91, 20);
            this.lblEndCreate.TabIndex = 12;
            this.lblEndCreate.Text = "Endurance:";
            // 
            // lblStrCreate
            // 
            this.lblStrCreate.AutoSize = true;
            this.lblStrCreate.Location = new System.Drawing.Point(272, 285);
            this.lblStrCreate.Name = "lblStrCreate";
            this.lblStrCreate.Size = new System.Drawing.Size(75, 20);
            this.lblStrCreate.TabIndex = 13;
            this.lblStrCreate.Text = "Strength:";
            // 
            // nudVit
            // 
            this.nudVit.Location = new System.Drawing.Point(353, 188);
            this.nudVit.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudVit.Name = "nudVit";
            this.nudVit.Size = new System.Drawing.Size(53, 26);
            this.nudVit.TabIndex = 14;
            this.nudVit.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // nudEnd
            // 
            this.nudEnd.Location = new System.Drawing.Point(353, 219);
            this.nudEnd.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudEnd.Name = "nudEnd";
            this.nudEnd.Size = new System.Drawing.Size(53, 26);
            this.nudEnd.TabIndex = 15;
            this.nudEnd.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // nudInt
            // 
            this.nudInt.Location = new System.Drawing.Point(353, 250);
            this.nudInt.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudInt.Name = "nudInt";
            this.nudInt.Size = new System.Drawing.Size(53, 26);
            this.nudInt.TabIndex = 16;
            this.nudInt.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // nudStr
            // 
            this.nudStr.Location = new System.Drawing.Point(353, 283);
            this.nudStr.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudStr.Name = "nudStr";
            this.nudStr.Size = new System.Drawing.Size(53, 26);
            this.nudStr.TabIndex = 17;
            this.nudStr.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // nudAgi
            // 
            this.nudAgi.Location = new System.Drawing.Point(353, 315);
            this.nudAgi.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudAgi.Name = "nudAgi";
            this.nudAgi.Size = new System.Drawing.Size(53, 26);
            this.nudAgi.TabIndex = 18;
            this.nudAgi.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(98, 17);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 20);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "name";
            // 
            // lblVit
            // 
            this.lblVit.AutoSize = true;
            this.lblVit.Location = new System.Drawing.Point(43, 213);
            this.lblVit.Name = "lblVit";
            this.lblVit.Size = new System.Drawing.Size(59, 20);
            this.lblVit.TabIndex = 20;
            this.lblVit.Text = "Vitality:";
            // 
            // lblAgi
            // 
            this.lblAgi.AutoSize = true;
            this.lblAgi.Location = new System.Drawing.Point(48, 340);
            this.lblAgi.Name = "lblAgi";
            this.lblAgi.Size = new System.Drawing.Size(54, 20);
            this.lblAgi.TabIndex = 21;
            this.lblAgi.Text = "Agility:";
            // 
            // lblInt
            // 
            this.lblInt.AutoSize = true;
            this.lblInt.Location = new System.Drawing.Point(33, 275);
            this.lblInt.Name = "lblInt";
            this.lblInt.Size = new System.Drawing.Size(69, 20);
            this.lblInt.TabIndex = 22;
            this.lblInt.Text = "Intellect:";
            // 
            // lblEnd
            // 
            this.lblEnd.AutoSize = true;
            this.lblEnd.Location = new System.Drawing.Point(11, 244);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(91, 20);
            this.lblEnd.TabIndex = 23;
            this.lblEnd.Text = "Endurance:";
            // 
            // lblClass
            // 
            this.lblClass.AutoSize = true;
            this.lblClass.Location = new System.Drawing.Point(98, 57);
            this.lblClass.Name = "lblClass";
            this.lblClass.Size = new System.Drawing.Size(45, 20);
            this.lblClass.TabIndex = 4;
            this.lblClass.Text = "class";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(66, 119);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 20);
            this.label8.TabIndex = 20;
            this.label8.Text = "HP: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 20);
            this.label7.TabIndex = 21;
            this.label7.Text = "Stamina:";
            // 
            // lblStr
            // 
            this.lblStr.AutoSize = true;
            this.lblStr.Location = new System.Drawing.Point(27, 308);
            this.lblStr.Name = "lblStr";
            this.lblStr.Size = new System.Drawing.Size(75, 20);
            this.lblStr.TabIndex = 24;
            this.lblStr.Text = "Strength:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(49, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 20);
            this.label6.TabIndex = 22;
            this.label6.Text = "Mana:";
            // 
            // lblVitShow
            // 
            this.lblVitShow.AutoSize = true;
            this.lblVitShow.Location = new System.Drawing.Point(132, 213);
            this.lblVitShow.Name = "lblVitShow";
            this.lblVitShow.Size = new System.Drawing.Size(18, 20);
            this.lblVitShow.TabIndex = 25;
            this.lblVitShow.Text = "0";
            // 
            // lblIntShow
            // 
            this.lblIntShow.AutoSize = true;
            this.lblIntShow.Location = new System.Drawing.Point(132, 275);
            this.lblIntShow.Name = "lblIntShow";
            this.lblIntShow.Size = new System.Drawing.Size(18, 20);
            this.lblIntShow.TabIndex = 26;
            this.lblIntShow.Text = "0";
            // 
            // lblEndShow
            // 
            this.lblEndShow.AutoSize = true;
            this.lblEndShow.Location = new System.Drawing.Point(132, 244);
            this.lblEndShow.Name = "lblEndShow";
            this.lblEndShow.Size = new System.Drawing.Size(18, 20);
            this.lblEndShow.TabIndex = 27;
            this.lblEndShow.Text = "0";
            // 
            // lblStrShow
            // 
            this.lblStrShow.AutoSize = true;
            this.lblStrShow.Location = new System.Drawing.Point(132, 308);
            this.lblStrShow.Name = "lblStrShow";
            this.lblStrShow.Size = new System.Drawing.Size(18, 20);
            this.lblStrShow.TabIndex = 28;
            this.lblStrShow.Text = "0";
            // 
            // lblAgiShow
            // 
            this.lblAgiShow.AutoSize = true;
            this.lblAgiShow.Location = new System.Drawing.Point(132, 342);
            this.lblAgiShow.Name = "lblAgiShow";
            this.lblAgiShow.Size = new System.Drawing.Size(18, 20);
            this.lblAgiShow.TabIndex = 29;
            this.lblAgiShow.Text = "0";
            // 
            // lblHPShow
            // 
            this.lblHPShow.AutoSize = true;
            this.lblHPShow.Location = new System.Drawing.Point(103, 120);
            this.lblHPShow.Name = "lblHPShow";
            this.lblHPShow.Size = new System.Drawing.Size(18, 20);
            this.lblHPShow.TabIndex = 30;
            this.lblHPShow.Text = "0";
            // 
            // lblStaminaShow
            // 
            this.lblStaminaShow.AutoSize = true;
            this.lblStaminaShow.Location = new System.Drawing.Point(103, 140);
            this.lblStaminaShow.Name = "lblStaminaShow";
            this.lblStaminaShow.Size = new System.Drawing.Size(18, 20);
            this.lblStaminaShow.TabIndex = 31;
            this.lblStaminaShow.Text = "0";
            // 
            // lblManaShow
            // 
            this.lblManaShow.AutoSize = true;
            this.lblManaShow.Location = new System.Drawing.Point(103, 160);
            this.lblManaShow.Name = "lblManaShow";
            this.lblManaShow.Size = new System.Drawing.Size(18, 20);
            this.lblManaShow.TabIndex = 32;
            this.lblManaShow.Text = "0";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.lblManaShow);
            this.panel1.Controls.Add(this.lblStaminaShow);
            this.panel1.Controls.Add(this.lblHPShow);
            this.panel1.Controls.Add(this.lblAgiShow);
            this.panel1.Controls.Add(this.lblStrShow);
            this.panel1.Controls.Add(this.lblEndShow);
            this.panel1.Controls.Add(this.lblIntShow);
            this.panel1.Controls.Add(this.lblVitShow);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lblStr);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lblClass);
            this.panel1.Controls.Add(this.lblEnd);
            this.panel1.Controls.Add(this.lblInt);
            this.panel1.Controls.Add(this.lblAgi);
            this.panel1.Controls.Add(this.lblVit);
            this.panel1.Controls.Add(this.lblName);
            this.panel1.Location = new System.Drawing.Point(541, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(247, 424);
            this.panel1.TabIndex = 5;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(88, 386);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 33);
            this.btnDelete.TabIndex = 33;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.nudAgi);
            this.Controls.Add(this.nudStr);
            this.Controls.Add(this.nudInt);
            this.Controls.Add(this.nudEnd);
            this.Controls.Add(this.nudVit);
            this.Controls.Add(this.lblStrCreate);
            this.Controls.Add(this.lblEndCreate);
            this.Controls.Add(this.lblIntCreate);
            this.Controls.Add(this.lblAgiCreate);
            this.Controls.Add(this.lblVitCreate);
            this.Controls.Add(this.cbmClassCreate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.lbCharacters);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lblAddName);
            this.Controls.Add(this.lblHeading);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudVit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAgi)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Label lblAddName;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.ListBox lbCharacters;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.ComboBox cbmClassCreate;
        private System.Windows.Forms.Label lblVitCreate;
        private System.Windows.Forms.Label lblAgiCreate;
        private System.Windows.Forms.Label lblIntCreate;
        private System.Windows.Forms.Label lblEndCreate;
        private System.Windows.Forms.Label lblStrCreate;
        private System.Windows.Forms.NumericUpDown nudVit;
        private System.Windows.Forms.NumericUpDown nudEnd;
        private System.Windows.Forms.NumericUpDown nudInt;
        private System.Windows.Forms.NumericUpDown nudStr;
        private System.Windows.Forms.NumericUpDown nudAgi;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblVit;
        private System.Windows.Forms.Label lblAgi;
        private System.Windows.Forms.Label lblInt;
        private System.Windows.Forms.Label lblEnd;
        private System.Windows.Forms.Label lblClass;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblStr;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblVitShow;
        private System.Windows.Forms.Label lblIntShow;
        private System.Windows.Forms.Label lblEndShow;
        private System.Windows.Forms.Label lblStrShow;
        private System.Windows.Forms.Label lblAgiShow;
        private System.Windows.Forms.Label lblHPShow;
        private System.Windows.Forms.Label lblStaminaShow;
        private System.Windows.Forms.Label lblManaShow;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDelete;
    }
}

