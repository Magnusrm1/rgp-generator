﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RPGClassLib
{
    public abstract class Character
    {
        public int CharacterID { get; set; }
        public int ClassID { get; set; }
        public string Name { get; set; }
        public int HpMax { get; set; }
        public int Hp { get; set; }
        public int StaminaMax { get; set; }
        public int Stamina { get; set; }
        public int ManaMax { get; set; }
        public int Mana { get; set; }
        public double ArmorRating { get; set; }
        public int Vitality { get; set; }
        public int Endurance { get; set; }
        public int Intellect { get; set; }
        public int Strength { get; set; }
        public int Agility { get; set; }


        public Character(string name, int vit, int end, int intellect, int str, int agi)
        {
            Name = name;
            Vitality = vit;
            Endurance = end;
            Intellect = intellect;
            Strength = str;
            Agility = agi;
            Stamina = 100;
            StaminaMax = 100;
            Hp = vit * 10;
            HpMax = vit * 10;
            Mana = intellect * 10;
            ManaMax = intellect * 10;
        }
        public Character(int characterID, string name, int vit, int end, int intellect, int str, int agi)
        {
            CharacterID = characterID;
            Name = name;
            Vitality = vit;
            Endurance = end;
            Intellect = intellect;
            Strength = str;
            Agility = agi;
            Stamina = 100;
            StaminaMax = 100;
            Hp = vit * 10;
            HpMax = vit * 10;
            Mana = intellect * 10;
            ManaMax = intellect * 10;
        }

        public abstract void Move();
        public abstract void Attack();
    }
}
