﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGClassLib
{
    public abstract class Thief : Character
    {

        public Thief(string name, int vit, int end, int intellect, int str, int agi) : base(name, vit, end, intellect, str, agi)
        {

        }

        public Thief(int characterID, string name, int vit, int end, int intellect, int str, int agi) : base(characterID, name, vit, end, intellect, str, agi)
        {

        }
    }
}
