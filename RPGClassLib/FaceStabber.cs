﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGClassLib
{
    public class FaceStabber : Thief
    {
        public FaceStabber(string name, int vit, int end, int intellect, int str, int agi) : base(name, vit, end, intellect, str, agi)
        {
            ClassID = 2;
        }

        public FaceStabber(int characterID, string name, int vit, int end, int intellect, int str, int agi) : base(characterID, name, vit, end, intellect, str, agi)
        {
            ClassID = 2;
        }
        public override void Attack()
        {
            throw new NotImplementedException();
        }

        public override void Move()
        {
            throw new NotImplementedException();
        }
    }
}
