﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGClassLib
{
    public abstract class Wizard : Character
    {

        public Wizard(string name, int vit, int end, int intellect, int str, int agi) : base(name, vit, end, intellect, str, agi)
        {

        }

        public Wizard(int characterID, string name, int vit, int end, int intellect, int str, int agi) : base(characterID, name, vit, end, intellect, str, agi)
        {

        }

    }
}
